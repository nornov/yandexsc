package yandexsc

import (
	"bytes"
	"encoding/binary"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/go-audio/wav"

	"github.com/go-audio/audio"
)

// YSK is the main struct with necessry methods
type YSK struct {
	Token, Folder string
}

// NewYSK creates a copy of YSK struct
func NewYSK(token, folder string) *YSK {
	return &YSK{
		Token:  token,
		Folder: folder,
	}
}

func (y *YSK) getRawFile(phrase, lang string) ([]byte, error) {
	client := http.Client{
		Timeout: 15 * time.Second,
	}
	form := url.Values{
		"text":            {phrase},
		"lang":            {lang},
		"voice":           {"oksana"},
		"emotion":         {"good"},
		"format":          {"lpcm"},
		"sampleRateHertz": {"8000"},
		"folderId":        {y.Folder},
	}
	req, err := http.NewRequest(
		"POST",
		"https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize",
		strings.NewReader(form.Encode()),
	)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Api-Key "+y.Token)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode > 300 {
		return nil, errors.New(string(body))
	}
	return body, nil
}

// GetWavFileForAster returns byte array with wav headers
func (y *YSK) GetWavFileForAster(phrase, lang, outFile string) error {
	raw, err := y.getRawFile(phrase, lang)
	if err != nil {
		return err
	}
	fOut, err := os.Create(outFile)
	if err != nil {
		return err
	}
	defer fOut.Close()
	e := wav.NewEncoder(fOut, 8000, 16, 1, 1)
	buf := bytes.NewReader(raw)
	audioBuf, err := newAudioIntBuffer(buf)
	if err != nil {
		return err
	}
	if err := e.Write(audioBuf); err != nil {
		return err
	}
	return e.Close()
}

func newAudioIntBuffer(r io.Reader) (*audio.IntBuffer, error) {
	buf := audio.IntBuffer{
		Format: &audio.Format{
			NumChannels: 1,
			SampleRate:  8000,
		},
		SourceBitDepth: 16,
	}
	for {
		var sample int16
		err := binary.Read(r, binary.LittleEndian, &sample)
		if err != nil {
			return &buf, nil
		}
		buf.Data = append(buf.Data, int(sample))
	}
}
